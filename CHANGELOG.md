# Change Log
## [0.1.14] - 2021-07.02
### Added
- Pandoc
- MiKTex and the complete MiKTeX package repository
- MSYS2
- Python 2.7.18
- Utility scripts to switch between Python2 and Python3
- "xlrd", "pandas", "html-testRunner" modules for Python3
- "Shader languages support for VS Code", "Markdown Preview Enhanced" and "LaTeX language support" extensions for VSCode
### Removed
- "CMake Tools Helper" extension for VSCode as it is not supported and not on the marketplace anymore
- Qt unattended install scripts as they are not needed anymore
### Changed
- MinGW compilers and binaries are now installed through MSYS2
- Updated Python3 to 3.9.4
- Updated Qt to 5.15.2
- Updated Cmder, Eclipse, Firefox, Git, Thunderbird, Notepad++, VSCode to their latest releases
- Updated VSCode extensions and Eclipse plugins to the latest releases
- FreeGLUT and GLEW for MSVC are now compiled, and aren't downloaded as binaries
- GDAL and rasterio wheel files can now be downloaded by the script
- Qt for MSVC is now installed via the Python3 package "aqtinstall"
- Fixed path breaking bugs in setenv.bat

## [0.1.13] - 2019-06.21
### Added
- Qt Libraries for msvc2013 32-bit
- freeglut for msvc2013
- glew for msvc2013
- More Eclipse plugins
- More Python modules
- Notepad++
- Imagemagick
- Utility scripts to switch between mingw and msvc
- Git History and GitLens plugins for vscode
### Changed
- Downgrade Qt to 5.8
- Upgrade other items
- Switch to use OpenJDK instead of JRE
- Move ninja to its own folder during extraction
- Remove 7zip dependency
- Improve provisioning files
- Remove powershell extension from vscode
- Remove groovy plugin from eclipse

## [0.1.12] - 2018-05.13
### Added
- Visual Studio Code
- Doxygen
- Firefox
- Thunderbird
- More Vrapper plugins to Eclipse
### Changed
- Remove Perl Language
- Remove Groovy Language
- Use 64-bit software with the latest versions except for Qt5 and svn
- Copy mingw32-make.exe to make.exe for convenience
- Install JRE installed of JDK
- Add CMAKE_PREFIX_PATH environment variable for qt5, freeglut and glew

## [0.1.11] - 2017-08.17
### Added
- Add glew library
### Changed
- Fix Eclipse Perl Plugin

## [0.1.10] - 2017-08.06
### Added
- Qt 5.9 for MinGW
- PyQt5
- Fix Python and Perl environment settings
- Give Eclipse more RAM than default
### Changed
- Switch from x64 to x86 for all items
- Switch from jre to jdk
- Use Qt5's compiler toolchain

## [0.1.9] - 2017-07.08
### Added
- Perl from strawberry-perl-5.26.0.1-64bit-portable.zip
### Changed
- Eclipse Platform Runtime Binary - eclipse-platform-4.7-win32-x86_64.zip
- Fix ninja provision check
- Replace subversive with subeclipse
- Remove AnyEditTools Eclipse Plugin
- Remove Eclipse Color Theme Plugin, use Eclipse Preferences import instead.

## [0.1.8] - 2017-07.01
### Added
- Eclipse Color Theme Plugin
- Ninja (Build System)
- WNIX_LIBS_PATH environment variable
### Changed
- Copy mingw32-make as make in provision
- Update Eclipse CMake Editor Plugin
- Remove Cmder PATHs from Eclipse startup

## [0.1.7] - 2017-07.01
### Added
- CMake (Build System Generator)
- Eclipse cmake4eclipse  and CMake Editor plugins 
- Eclipse Markdown Text Editor plugin
### Changed
- Rename mingw32-make to make in provision
- Phases clarified in README.md

## [0.1.6] - 2017-06.21
### Changed
- Fix shortcuts script

## [0.1.5] - 2017-06.20
### Changed
- We now have python3 instead of python2
- Fix Perl path
- Fix install script
- Put Unix commands before the Windows's in PATH

## [0.1.4] - 2017-06.20
### Added
- Eclipse: AnyEditTools Plugin for working set import/export
### Changed
- Do not rename mingw32-make to make during provisioning

## [0.1.3] - 2017-06.18
### Added
- Eclipse: DXL Editor Plugin
### Changed
- Eclipse Vrapper Plugin is now optional during installation

## [0.1.2] - 2017-05.28
### Changed
- Eclipse SvnKit installation fixed
- Redundant PATHs deleted

## [0.1.1] - 2017-05.27
### Added
- Automation script for downloading and provisioning the components
- Python 2.7.13
- Groovy 1.6.3
- Eclipse: PyDev - Python Development Plugin
- Eclipse: EPIC  - Perl Development Plugin
- Eclipse: Groovy-Eclipse - Groovy Development Plugin
- Eclipse: Subversive - SVN Plugin
- SVN CLI 1.8.17
### Removed
- Eclipse: Marketplace Client Plugin
### Changed
- MinGW-w64 - x86_64-7.1.0-release-posix-seh-rt_v5-rev0.7z

## [0.1.0] - 2017-05-19
### Added
- Eclipse Platform Runtime Binary - eclipse-platform-4.6.3-win32-x86_64.zip
- Eclipse: Marketplace Client Plugin
- Eclipse: C/C++ Development Tools Plugin
- Eclipse: TM Terminal Plugin
- Eclipse: Vrapper Plugin
- Java Runtime Environment - jre-8u131-windows-x64.tar.gz
- MinGW-w64 - x86_64-7.1.0-release-posix-sjlj-rt_v5-rev0.7z
- Cmder v1.3.2 with git-for-windows (Also has MSYS and Perl 5.24.1)

