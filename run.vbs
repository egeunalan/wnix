Set oShell = CreateObject("WScript.Shell")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.GetFile(Wscript.ScriptFullName)
strFolder = objFSO.GetParentFolderName(objFile)
strApp = WScript.Arguments(1)

' Run bat as hidden
oShell.Run("""" & strFolder & "\runner.bat"" " & strApp), 0, True
