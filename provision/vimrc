" Setting some decent VIM settings for programming

set ai                          " set auto-indenting on for programming
set showmatch                   " automatically show matching brackets. works like it does in bbedit.
set vb                          " turn on the "visual bell" - which is much quieter than the "audio blink"
set ruler                       " show the cursor position all the time
set laststatus=2                " make the last line where the status is two lines deep so you can see status always
set backspace=indent,eol,start  " make that backspace key work the way it should
set nocompatible                " vi compatible is LAME
set background=dark             " Use colours that work well on a dark background (Console is usually black)
set showmode                    " show the current mode
set clipboard=unnamed           " set clipboard to unnamed to access the system clipboard under windows
set incsearch                   " Shows the match while typing
set hlsearch                    " Highlight found text
set mouse=a                     " Enables mouse in all modes
set wildmenu                    " Completion with menu
set number                      " Show line numbers
set nobackup                    " No backup before overwriting a file
set noswapfile                  " No *.swp files
set splitright                  " Opens vertical split right of current window
set splitbelow                  " Opens horizontal split below current window
set shortmess=a                 " Use short messages when interacting with user
set foldlevelstart=0            " Folds are closed by default
set wildmode=longest,list,full  " Tab presses cycle through these modes
set wildignore=*.o,*.obj,*~     " Stuff to ignore when tab completing
set noerrorbells visualbell t_vb= "No bell noise on errors
autocmd GUIEnter * set visualbell t_vb= "No visual noise errors
set lazyredraw                  " Eliminate redundant redraws
set cmdheight=2                 " Avoid many cases of having to press <Enter> to continue
set history=1000                " Store lots of cmdline history
set undolevels=1000             " Use many undo levels
set hidden                      " Buffers can exist in the background
set scrolloff=10                " Keep some lines visible for vertical margins
set sidescrolloff=5             " Keep some columns visible for horizontal margins
set virtualedit=block           " Useful for selecting a rectangle
set ignorecase                  " Case insesitive search
set smartcase                   " Case sensitive search when an upper case typed
set wrapscan                    " Wrap again after the last match
set tabstop=4                   " Number of spaces a tab counts for
set shiftwidth=4                " Spaces for autoindents
set softtabstop=4               " Tab spaces to be inserted when hit
set shiftround                  " Makes indenting a multiple of shiftwidth
set expandtab                   " Turn a tab into spaces
set copyindent                  " Copy the previous indentation on autoindenting
set textwidth=79                " Enforce line length
set colorcolumn=+1              " Highlight the limit of textwidth
set wrap                        " Wrap long text in display	
syntax on                       " turn syntax highlighting on by default

" Show EOL type and last modified timestamp, right after the filename
set statusline=%<%F%h%m%r\ [%{&ff}]\ (%{strftime(\"%H:%M\ %d/%m/%Y\",getftime(expand(\"%:p\")))})%=%l,%c%V\ %P

"------------------------------------------------------------------------------
" Only do this part when compiled with support for autocommands.
if has("autocmd")
    "Set UTF-8 as the default encoding for commit messages
    autocmd BufReadPre COMMIT_EDITMSG,git-rebase-todo setlocal fileencodings=utf-8

    "Remember the positions in files with some git-specific exceptions"
    autocmd BufReadPost *
      \ if line("'\"") > 0 && line("'\"") <= line("$")
      \           && expand("%") !~ "COMMIT_EDITMSG"
      \           && expand("%") !~ "ADD_EDIT.patch"
      \           && expand("%") !~ "addp-hunk-edit.diff"
      \           && expand("%") !~ "git-rebase-todo" |
      \   exe "normal g`\"" |
      \ endif

      autocmd BufNewFile,BufRead *.patch set filetype=diff
      autocmd BufNewFile,BufRead *.diff set filetype=diff

      autocmd Syntax diff
      \ highlight WhiteSpaceEOL ctermbg=red |
      \ match WhiteSpaceEOL /\(^+.*\)\@<=\s\+$/

      autocmd Syntax gitcommit setlocal textwidth=74
endif " has("autocmd")

noremap <silent> - :Explore<CR>
