# WNIX

WNIX is a UNIX-like Windows Development Environment. It contains one of the
latest versions of  GNU toolchain, must-have GNU command line utilities,
popular scripting languages, Eclipse IDE, and famous version control systems.

## Quick Start Guide

Download the latest version and extract it to any location you wish.  Run
`install.bat`.  When the install completes, several shortcuts
should appear on your Start Menu.  You can disable vim plugins from the editors
or IDEs if you don't like it, but you would miss a great oppurtunity.

If you don't want to / can't use the shortcuts, you can run the programs directly from the WNIX installation folder by using runner.bat
``` bash
runner.bat cmder        # For Cmder
runner.bat eclipse      # For Eclipse
runner.bat code         # For VSCode
runner.bat qtcreator    # For Qt Creator
runner.bat qtdesigner   # For Qt Designer
```
For other programs such as Firefox or Notepad++ you can directly run them from their executables as they don't depend on any environment variables.

If you need to change python version just run the following commands from Cmder or after running setenv.bat in a terminal session:
``` bash
swpy2
python --version
# Python 2.7.18
pip --version
# pip 20.3.4 from D:/wnix/python2/lib/site-packages/pip (python 2.7)
swpy3
python --version
# Python 3.9.4
pip --version
# pip 21.1.3 from D:/wnix/python3/lib/site-packages/pip (python 3.9)
```

If you need to change your compiler between mingw and msvc run the following commands from Cmder or after running setenv.bat in a terminal session:
``` bash
# Switch to mingw
mingw
# Switch to msvc
msvc
```

Note that the defaults are Python3 and mingw. Once you run the above commands,
you need to restart applications in order to activate proper
environment setup except for the current command line session.

## Creating the Environment from Scratch

You first need to
[download](https://www.msys2.org/), 
and install msys2. Select `wnix/msys2` as installation folder. Run 
`wnix/msys2/mingw32.exe`, and execute following command.
``` bash
pacman -Syu --noconfirm
```
The terminal will exit, reopen the termimnal, and execute the above command again.
Then navigate to your WNIX install directory with
``` bash
cd "C:\path\to\your\wnix\install\directory"
```

Then you can execute `wnixr.sh` with
``` bash
bash wnixr.sh
```

The script will try to download the wheel files for GDAL and rasterio, but if it fails you should manually [download](https://www.lfd.uci.edu/~gohlke/pythonlibs) the following wheel files and put them into `wnix/download`. See `wnixr.sh` file for details.
- GDAL‑3.3.0‑cp39‑cp39‑win_amd64.whl
- rasterio‑1.2.6‑cp39‑cp39‑win_amd64.whl

Once you have all these utilities, you can run `wnixr.sh` script in Cmder as
shown below. 

`sh wnixr.sh -h` is your friend.

``` bash
sh wnixr.sh -h
Usage: wnixr.sh [options]

Options
   -h, --help           Prints this help message
   -d, --download       Downloads programs
   -x, --extract        Extracts downloaded programs
   -p, --provision      Provisions extracted programs
   -c CMAKE_GENERATOR, --compile CMAKE_GENERATOR        Compile FreeGLUT and GLEW from source with the specified CMake generator
   -r, --release        Releases a tarball with extracted programs
```

When you want to create a new release, first make your changes then bump
the version number in `wnixr.sh` and finally run the following command.

``` bash
sh wnixr.sh -d -x -p -c CMAKE_GENERATOR -r
```

To compile FreeGLUT and GLEW binaries for MSVC you need to specify the `-c CMAKE_GENERATOR` option with a valid CMake generator. So you should have an MSVC compiler on your computer.
``` bash
"Visual Studio 16" For VS2019
"Visual Studio 15" For VS2017
"Visual Studio 14" For VS2015
"Visual Studio 12" For VS2013
```

The binaries can also be compiled afterwards with the `-c CMAKE_GENERATOR` option.

If anything goes wrong, delete the files or directories created by the last
operation and issue the above command again. It picks up where it left off
except for provisioning phase. The script creates *.download*, *.extract* and
*.provision* files to indicate the completion of the corresponding phase so
that the next phase is good to go. For example release phase can begin only if
*.provision* file exists.
