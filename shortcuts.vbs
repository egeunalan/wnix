set oShell = WScript.CreateObject("WScript.Shell" )
strDesktop = oShell.SpecialFolders("Desktop" )
strPrograms = oShell.SpecialFolders("Programs")
strProfile = oShell.ExpandEnvironmentStrings("%USERPROFILE%")
strWnix = oShell.ExpandEnvironmentStrings("%WNIX_HOME%")

' Cmder
strCmder = oShell.ExpandEnvironmentStrings("%CMDER_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\Cmder.lnk" )
oShortcut.TargetPath = strWnix & "run.vbs"
oShortcut.Arguments = "--app cmder"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strCmder & "\Cmder.exe"
oShortcut.Description = "Run Cmder"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' Eclipse
strEclipse = oShell.ExpandEnvironmentStrings("%ECLIPSE_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\Eclipse.lnk" )
oShortcut.TargetPath = strWnix & "run.vbs"
oShortcut.Arguments = "--app eclipse"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strEclipse & "\eclipse.exe"
oShortcut.Description = "Run Eclipse"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' VScode
strCode = oShell.ExpandEnvironmentStrings("%VSCODE_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\VSCode.lnk" )
oShortcut.TargetPath = strWnix & "run.vbs"
oShortcut.Arguments = "--app code"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strCode & "\Code.exe"
oShortcut.Description = "Run VSCode"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' QtCreator
strQtCreator = oShell.ExpandEnvironmentStrings("%MINGW32_HOME%")
set oShortcut = oShell.CreateShortcut(strPrograms & "\QtCreator.lnk" )
oShortcut.TargetPath = strWnix & "run.vbs"
oShortcut.Arguments = "--app qtcreator"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strQtCreator & "\bin\qtcreator.exe"
oShortcut.Description = "Run QtCreator"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' QtDesigner
strQtDesigner = strWnix & "msys2\mingw32"
set oShortcut = oShell.CreateShortcut(strPrograms & "\QtDesigner.lnk" )
oShortcut.TargetPath = strWnix & "run.vbs"
oShortcut.Arguments = "--app qtdesigner"
oShortcut.WindowStyle = 1
oShortcut.IconLocation = strQtDesigner & "\bin\designer.exe"
oShortcut.Description = "Run QtDesigner"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' Thunderbird
set oShortcut = oShell.CreateShortcut(strPrograms & "\Thunderbird.lnk" )
oShortcut.TargetPath = strWnix & "thunderbird" & "/thunderbird.exe"
oShortcut.WindowStyle = 1
oShortcut.Description = "Run Thunderbird"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save

' Firefox
set oShortcut = oShell.CreateShortcut(strPrograms & "\Firefox.lnk" )
oShortcut.TargetPath = strWnix & "firefox" & "/firefox.exe"
oShortcut.WindowStyle = 1
oShortcut.Description = "Run Firefox"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save


' Notepad++
set oShortcut = oShell.CreateShortcut(strPrograms & "\Notepad++.lnk" )
oShortcut.TargetPath = strWnix & "npp" & "/notepad++.exe"
oShortcut.WindowStyle = 1
oShortcut.Description = "Run Notepad++"
oShortcut.WorkingDirectory = strProfile
oShortcut.Save
