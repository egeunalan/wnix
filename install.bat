@echo off

call "%~dp0setenv.bat"

:: Create msys2 directory link
REM set target=%~dp0msys2
REM if exist %MSYS2_HOME% rmdir %MSYS2_HOME%
REM mklink /D %MSYS2_HOME% %target% > nul
REM echo Created MSYS2 Link

REM :: Set QT Paths Prefix
REM echo QT Paths patching...
REM qtbinpatcher --qt-dir=%MINGW64_HOME% --nobackup > nul
REM echo Patched QT Paths

:: Create shortcuts
call "%~dp0shortcuts.vbs"
echo Created Shortcuts

echo Done!
pause
