@echo off

:: Preserve the initial path before any additions
if "%INITIAL_PATH%" == "" set INITIAL_PATH=%PATH%

:: Development environment root
if "%WNIX_HOME%" == "" set WNIX_HOME=%~dp0
set PATH=%WNIX_HOME%;%PATH%

:: Set active environment, defaults to mingw if not set before
if exist "%WNIX_HOME%.mingw" (
    set ACTIVE_ENV=mingw
) else if exist "%WNIX_HOME%.msvc" (
    set ACTIVE_ENV=msvc
) else (
    copy /y nul "%WNIX_HOME%.mingw" >nul
    set ACTIVE_ENV=mingw
)

:: Set active python version, defaults to python3 if not set before
if exist "%WNIX_HOME%.py3" (
    set PY_VERSION=3
) else if exist "%WNIX_HOME%.py2" (
    set PY_VERSION=2
) else (
    copy /y nul "%WNIX_HOME%.py3" >nul
    set PY_VERSION=3
)

if %ACTIVE_ENV% == msvc (
    :: Qt5
    set QT5LIBS_HOME=%WNIX_HOME%qt\5.15.2\msvc2019
    :: FreeGLUT
    set FREEGLUT_HOME=%WNIX_HOME%msvc\freeglut
    :: Glew
    set GLEW_HOME=%WNIX_HOME%msvc\glew
)

:: Msys2 environment root which is being created as link by install.bat
:: set MSYS2_HOME=C:\msys64
set MSYS2_HOME=%WNIX_HOME%msys2
set MINGW32_HOME=%MSYS2_HOME%\mingw32

set SCRIPT_HOME=%WNIX_HOME%scripts

:: Cmder Terminal Emulator
set CMDER_HOME=%WNIX_HOME%cmder

:: Git for Windows
set GIT_HOME=%WNIX_HOME%git

:: Java Development Kit
set JAVA_HOME=%WNIX_HOME%jdk

:: Visual Studio Code
set VSCODE_HOME=%WNIX_HOME%vscode

:: Svn
set SVN_HOME=%WNIX_HOME%svn

:: Pandoc
set PANDOC_HOME=%WNIX_HOME%pandoc

:: Miktex
set MIKTEX_HOME=%WNIX_HOME%miktex

:: Eclipse
set ECLIPSE_HOME=%WNIX_HOME%eclipse

:: Add msys bins to Path
set PATH=%MINGW32_HOME%\bin\;%PATH%

if %ACTIVE_ENV% == msvc (
    set PATH=%QT5LIBS_HOME%\bin;%FREEGLUT_HOME%\bin;%GLEW_HOME%\bin;%PATH%
    :: CMAKE
    set CMAKE_PREFIX_PATH=%QT5LIBS_HOME%;%FREEGLUT_HOME%;%GLEW_HOME%
) else (
    set CMAKE_PREFIX_PATH=
)

set PATH=%SCRIPT_HOME%;%PATH%
set PATH=%CMDER_HOME%\cmd;%CMDER_HOME%\vendor\conemu-maximus5\ConEmu\Scripts;%CMDER_HOME%\vendor\conemu-maximus5;%CMDER_HOME%\vendor\conemu-maximus5\ConEmu;%PATH%%GIT_HOME%\mingw64\bin;%GIT_HOME%\usr\bin;
set PATH=%GIT_HOME%\cmd;%PATH%
set PATH=%JAVA_HOME%\bin;%PATH%
set PATH=%VSCODE_HOME%\bin;%PATH%
set PATH=%SVN_HOME%\bin;%PATH%
set PATH=%PANDOC_HOME%\bin;%PATH%
set PATH=%MIKTEX_HOME%\texmfs\install\miktex\bin\x64;%PATH%
set PATH=%ECLIPSE_HOME%;%PATH%

:: Python path
set PATH=%WNIX_HOME%python%PY_VERSION%;%WNIX_HOME%python%PY_VERSION%\Scripts;%PATH%

:: Misc
set PYTHONIOENCODING=UTF-8
set LANG=C.UTF-8
