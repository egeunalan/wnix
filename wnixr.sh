#!/bin/sh

set -e

wnix_version="v0.1.14"

git_link=https://github.com/git-for-windows/git/releases/download/v2.32.0.windows.1/PortableGit-2.32.0-64-bit.7z.exe
cmder_link=https://github.com/cmderdev/cmder/releases/download/v1.3.18/cmder_mini.zip
jdk_link=https://download.java.net/openjdk/jdk11/ri/openjdk-11+28_windows-x64_bin.zip
eclipse_link=http://mirror.csclub.uwaterloo.ca/eclipse/eclipse/downloads/drops4/R-4.20-202106111600/eclipse-platform-4.20-win32-x86_64.zip
npp_link=https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v8.1/npp.8.1.portable.x64.zip
thunderbird_link=http://ftp.mozilla.org/pub/thunderbird/nightly/latest-comm-central/thunderbird-91.0a1.en-US.win64.zip
firefox_link=http://ftp.mozilla.org/pub/firefox/candidates/90.0b8-candidates/build1/win64/en-US/firefox-90.0b8.zip
vscode_link=https://go.microsoft.com/fwlink/?Linkid=850641
svn_link=http://deac-riga.dl.sourceforge.net/project/win32svn/1.8.17/apache22/svn-win32-1.8.17.zip
freeglut_msvc_link=https://github.com/dcnieho/FreeGLUT/archive/refs/tags/FG_3_2_1.zip
glew_msvc_link=https://github.com/nigels-com/glew/releases/download/glew-2.2.0/glew-2.2.0.zip
python2_link=https://www.python.org/ftp/python/2.7.18/python-2.7.18.amd64.msi
python3_link=https://github.com/winpython/winpython/releases/download/4.1.20210417/Winpython64-3.9.4.0dot.exe
miktex_link=https://miktex.org/download/ctan/systems/win32/miktex/setup/windows-x64/basic-miktex-21.6-x64.exe
miktex_repo_link=https://miktex.org/download/ctan/systems/win32/miktex/setup/windows-x64/miktexsetup-4.1-x64.zip
pandoc_link=https://github.com/jgm/pandoc/releases/download/2.14.0.2/pandoc-2.14.0.2-windows-x86_64.zip
svnkit_link=https://www.svnkit.com/org.tmatesoft.svn_1.10.3.eclipse.zip
subclipse_link=https://github.com/subclipse/subclipse/releases/download/4.3.3/subclipse-4.3.3.zip
cmake4eclipse_link=https://dl.cloudsmith.io/public/15knots/p2-zip/raw/files/cmake4eclipse-2.1.4.zip
cmakeed_link=https://dl.cloudsmith.io/public/15knots/p2-zip/raw/files/CMakeEd-1.17.0.zip

git_file=$(basename $git_link)
cmder_file=$(basename $cmder_link)
jdk_file=$(basename $jdk_link)
eclipse_file=$(basename $eclipse_link)
npp_file=$(basename $npp_link)
thunderbird_file=$(basename $thunderbird_link)
firefox_file=$(basename $firefox_link)
vscode_file=vscode.zip
svn_file=$(basename $svn_link)
freeglut_msvc_file=$(basename $freeglut_msvc_link)
glew_msvc_file=$(basename $glew_msvc_link)
python2_file=$(basename $python2_link)
python3_file=$(basename $python3_link)
gdal_file=GDAL-3.3.0-cp39-cp39-win_amd64.whl
rasterio_file=rasterio-1.2.4-cp39-cp39-win_amd64.whl
miktex_file=miktex-portable.exe
miktex_repo_file=$(basename $miktex_repo_link)
pandoc_file=$(basename $pandoc_link)
svnkit_file=$(basename $svnkit_link)
subclipse_file=$(basename $subclipse_link)
cmake4eclipse_file=$(basename $cmake4eclipse_link)
cmakeed_file=$(basename $cmakeed_link)

wnix_path=$(dirname "$(realpath $0)")
download_path="$wnix_path/download"
extract_path="$wnix_path"
provision_path="$wnix_path/provision"

download_check="$wnix_path/.download"
extract_check="$wnix_path/.extract"
provision_check="$wnix_path/.provision"

download() {
    mkdir -p "$download_path"
    mkdir -p "$download_path/eclipse_plugins"
    
    # Install extracting tools
    pacman -S p7zip zip unzip tar unrar --noconfirm --needed
    
    if [ ! -f "$download_path/$git_file" ]; then
        echo "Downloading git ..."
        curl -L -o "$download_path/$git_file" -k $git_link
    fi
    
    if [ ! -f "$download_path/$cmder_file" ]; then
        echo "Downloading cmder ..."
        curl -L -o "$download_path/$cmder_file" -k $cmder_link
    fi
    
    if [ ! -f "$download_path/$jdk_file" ]; then
        echo "Downloading jdk ..."
        curl -L -o "$download_path/$jdk_file" -k $jdk_link
    fi

    if [ ! -f "$download_path/$eclipse_file" ]; then
        echo "Downloading eclipse ..."
        curl -L -o "$download_path/$eclipse_file" -k $eclipse_link
    fi
    
    if [ ! -f "$download_path/$svn_file" ]; then
        echo "Downloading svn cli ..."
        curl -L -o "$download_path/$svn_file" -k $svn_link
    fi

    if [ ! -f "$download_path/$vscode_file" ]; then
        echo "Downloading vscode ..."
        curl -L -o "$download_path/$vscode_file" -k $vscode_link
    fi

    if [ ! -f "$download_path/$thunderbird_file" ]; then
        echo "Downloading thunderbird ..."
        curl -L -o "$download_path/$thunderbird_file" -k $thunderbird_link
    fi

    if [ ! -f "$download_path/$firefox_file" ]; then
        echo "Downloading firefox ..."
        curl -L -o "$download_path/$firefox_file" -k $firefox_link
    fi

    if [ ! -f "$download_path/$npp_file" ]; then
        echo "Downloading notepad++ ..."
        curl -L -o "$download_path/$npp_file" -k $npp_link
    fi

    if [ ! -f "$download_path/$freeglut_msvc_file" ]; then
        echo "Downloading freeglut msvc ..."
        curl -L -o "$download_path/$freeglut_msvc_file" -k $freeglut_msvc_link
    fi

    if [ ! -f "$download_path/$glew_msvc_file" ]; then
        echo "Downloading glew msvc ..."
        curl -L -o "$download_path/$glew_msvc_file" -k $glew_msvc_link
    fi
    
    if [ ! -f "$download_path/$python2_file" ]; then
        echo "Downloading python2 ..."
        curl -L -o "$download_path/$python2_file" -k $python2_link
    fi

    if [ ! -f "$download_path/$python3_file" ]; then
        echo "Downloading python3 ..."
        curl -L -o "$download_path/$python3_file" -k $python3_link
    fi

    # we cannot get a direct download link from the site, the links are generated by javascript and change after some time
    # so we have to get the needed values from the page and generate the link ourselves
    if [ ! -f "$download_path/$gdal_file" ]; then
        echo "Downloading GDAL whl ..."
        dl_page=$(curl -L -k https://www.lfd.uci.edu/~gohlke/pythonlibs/)
        ml=$(grep 'GDAL&#8209;3&#46;3&#46;0&#8209;cp39&#8209;cp39&#8209;win_' <<< "$dl_page" | grep -o '\[.*\],')
        mi=$(grep 'GDAL&#8209;3&#46;3&#46;0&#8209;cp39&#8209;cp39&#8209;win_' <<< "$dl_page" | grep -o '\".*\")')
        mi=$(echo $mi | sed -e 's/[")]//g' -e 's/&lt;/</g' -e 's/&#62;/>/g' -e 's/&#38;/&/g')
        IFS=',[]' read -r -a array <<< "$ml"
        gdal_link="https://download.lfd.uci.edu/pythonlibs/"
        while read -n 1 c ; do
            gdal_link=$gdal_link$(printf "\x$(printf %x ${array[$(($(($(printf %d\\n \'$c) - 47)) +1))]})")
        done < <(echo -n "$mi")
        curl -L -o "$download_path/$gdal_file" -k $gdal_link
    fi

    if [ ! -f "$download_path/$rasterio_file" ]; then
        echo "Downloading rasterio whl ..."
        dl_page=$(curl -L -k https://www.lfd.uci.edu/~gohlke/pythonlibs/)
        ml=$(grep 'rasterio&#8209;1&#46;2&#46;6&#8209;cp39&#8209;cp39&#8209;win_' <<< "$dl_page" | grep -o '\[.*\],')
        mi=$(grep 'rasterio&#8209;1&#46;2&#46;6&#8209;cp39&#8209;cp39&#8209;win_' <<< "$dl_page" | grep -o '\".*\")')
        mi=$(echo $mi | sed -e 's/[")]//g' -e 's/&lt;/</g' -e 's/&#62;/>/g' -e 's/&#38;/&/g')
        IFS=',[]' read -r -a array <<< "$ml"
        rasterio_link="https://download.lfd.uci.edu/pythonlibs/"
        while read -n 1 c ; do
            rasterio_link=$rasterio_link$(printf "\x$(printf %x ${array[$(($(($(printf %d\\n \'$c) - 47)) +1))]})")
        done < <(echo -n "$mi")
        curl -L -o "$download_path/$rasterio_file" -k $rasterio_link
    fi

    if [ ! -f "$download_path/$miktex_file" ]; then
        echo "Downloading miktex ..."
        curl -L -o "$download_path/$miktex_file" -k $miktex_link
    fi
    
    if [ ! -d "$download_path/miktex-repo" ] && [ ! -d "$extract_path/miktex/miktex_repo" ]; then
        echo "Downloading the complete miktex repository, this may take a while ..."
        curl -L -o "$download_path/$miktex_repo_file" -k $miktex_repo_link
        unzip "$download_path/$miktex_repo_file" -d "$download_path"
        "$download_path/miktexsetup_standalone" --local-package-repository="$download_path/miktex-repo" --package-set=complete --quiet download
    fi

    if [ ! -f "$download_path/$pandoc_file" ]; then
        echo "Downloading pandoc ..."
        curl -L -o "$download_path/$pandoc_file" -k $pandoc_link
    fi

    if [ ! -f "$download_path/eclipse_plugins/$svnkit_file" ]; then
        echo "Downloading svnkit ..."
        curl -L -o "$download_path/eclipse_plugins/$svnkit_file" -k $svnkit_link
    fi
    
    if [ ! -f "$download_path/eclipse_plugins/$subclipse_file" ]; then
        echo "Downloading subclipse ..."
        curl -L -o "$download_path/eclipse_plugins/$subclipse_file" -k $subclipse_link
    fi    
    
    if [ ! -f "$download_path/eclipse_plugins/$cmake4eclipse_file" ]; then
        echo "Downloading cmake4eclipse ..."
        curl -L -o "$download_path/eclipse_plugins/$cmake4eclipse_file" -k $cmake4eclipse_link
    fi    
    
    if [ ! -f "$download_path/eclipse_plugins/$cmakeed_file" ]; then
        echo "Downloading cmakeed ..."
        curl -L -o "$download_path/eclipse_plugins/$cmakeed_file" -k $cmakeed_link
    fi

    touch "$download_check"
}

extract() {
    mkdir -p "$extract_path"
    mkdir -p "$extract_path/src"
    
    # git
    if [ ! -d "$extract_path/git" ]; then
        mkdir -p "$extract_path/git"
        7za x "$download_path/$git_file" -o"$extract_path/git"
    fi
    
    # cmder
    if [ ! -d "$extract_path/cmder" ]; then
        mkdir -p "$extract_path/cmder"
        unzip "$download_path/$cmder_file" -d "$extract_path/cmder"
    fi
    
    # jdk
    if [ ! -d "$extract_path/jdk" ]; then
        unzip "$download_path/$jdk_file" -d "$extract_path"
        mv "$extract_path/"*"jdk"* "$extract_path/jdk"
    fi

    # eclipse
    if [ ! -d "$extract_path/eclipse" ]; then
        unzip "$download_path/$eclipse_file" -d "$extract_path"
    fi

    # svn
    if [ ! -d "$extract_path/svn" ]; then
        unzip "$download_path/$svn_file" -d "$extract_path"
        mv "$extract_path/svn"* "$extract_path/svn"
    fi

    # vscode
    if [ ! -d "$extract_path/vscode" ]; then
        mkdir -p "$extract_path/vscode"
        unzip "$download_path/$vscode_file" -d "$extract_path/vscode"
    fi

    # thunderbird
    if [ ! -d "$extract_path/thunderbird" ]; then
        unzip "$download_path/$thunderbird_file" -d "$extract_path"
    fi

    # firefox
    if [ ! -d "$extract_path/firefox" ]; then
        unzip "$download_path/$firefox_file" -d "$extract_path"
    fi

    # notepad++
    if [ ! -d "$extract_path/npp" ]; then
        mkdir -p "$extract_path/npp"
        unzip "$download_path/$npp_file" -d "$extract_path/npp"
    fi

    # python2
    if [ ! -d "$extract_path/python2" ]; then
        echo "msiexec /a \"$(cygpath -w "$download_path/$python2_file")\" TARGETDIR=\"$(cygpath -w "$extract_path/python2")\" /qn" | cmd
    fi

    # python3
    if [ ! -d "$extract_path/python3" ]; then
        7z x "$download_path/$python3_file" -o"$extract_path" python-3.9.4.amd64 -r
        mv "$extract_path/WPy64-3940/python-3.9.4.amd64" "$extract_path/python3"
        rm -r "$extract_path/WPy64-3940"
    fi

    # freeglut msvc
    if [ ! -d "$extract_path/src/freeglut" ]; then
        unzip "$download_path/$freeglut_msvc_file" -d "$extract_path/src"
        mv "$extract_path/src/FreeGLUT-FG_3_2_1" "$extract_path/src/freeglut"
    fi

    # glew msvc
    if [ ! -d "$extract_path/src/glew" ]; then
        unzip -o "$download_path/$glew_msvc_file" -d "$extract_path/src"
        mv "$extract_path/src/glew-2.2.0" "$extract_path/src/glew"
    fi
    
    # pandoc
    if [ ! -d "$extract_path/pandoc" ]; then
        unzip -j -o "$download_path/$pandoc_file" -d "$extract_path/pandoc"
    fi

    # svnkit
    if [ ! -d "$download_path/eclipse_plugins/svnkit" ]; then
        unzip -o "$download_path/eclipse_plugins/$svnkit_file" -d "$download_path/eclipse_plugins/svnkit"
    fi
    
    # subclipse
    if [ ! -d "$download_path/eclipse_plugins/subclipse" ]; then
        unzip -o "$download_path/eclipse_plugins/$subclipse_file" -d "$download_path/eclipse_plugins/subclipse"
    fi

    # cmake4eclipse
    if [ ! -d "$download_path/eclipse_plugins/cmake4eclipse" ]; then
        unzip -o "$download_path/eclipse_plugins/$cmake4eclipse_file" -d "$download_path/eclipse_plugins/cmake4eclipse"
    fi

    # cmakeed
    if [ ! -d "$download_path/eclipse_plugins/cmakeed" ]; then
        unzip -o "$download_path/eclipse_plugins/$cmakeed_file" -d "$download_path/eclipse_plugins/cmakeed"
    fi

    # Reload msys2 pacman cache
    if [ -d "$download_path/cache" ]; then
        cp -r "$download_path/cache/pacman" /var/cache
    fi

    touch "$extract_check"
}

install_eclipse_plugins() {
    echo "Installing eclipse plugins"
    # eclipse
    repoUrls=http://download.eclipse.org/releases/2021-06
    repoUrls=$repoUrls,http://download.eclipse.org/eclipse/updates/4.20

    # c/c++
    repoUrls=$repoUrls,http://download.eclipse.org/tools/cdt/releases/9.10
    features=org.eclipse.cdt.feature.group

    # terminal
    repoUrls=$repoUrls,http://download.eclipse.org/tm/terminal/marketplace
    features=$features,org.eclipse.tm.terminal.feature.feature.group

    # pydev
    repoUrls=$repoUrls,http://pydev.sourceforge.net/pydev_update_site/8.3.0/
    features=$features,org.python.pydev.feature.feature.group

    # vrapper
    repoUrls=$repoUrls,http://vrapper.sourceforge.net/update-site/stable
    features=$features,net.sourceforge.vrapper.feature.group
    features=$features,net.sourceforge.vrapper.plugin.splitEditor.feature.group

    # perl
    repoUrls=$repoUrls,http://www.epic-ide.org/updates/testing
    features=$features,org.epic.feature.main.feature.group

    # java
    features=$features,org.eclipse.jdt.feature.group

    # svn
    repoUrls=$repoUrls,"\"file:///$(cygpath -m "$download_path/eclipse_plugins/svnkit")\"","\"file:///$(cygpath -m "$download_path/eclipse_plugins/subclipse")\""
    features=$features,org.tmatesoft.svnkit.feature.group
    features=$features,org.tigris.subversion.clientadapter.svnkit.feature.feature.group
    features=$features,org.tigris.subversion.subclipse.feature.group
    features=$features,org.tigris.subversion.subclipse.graph.feature.feature.group

    # dxl editor
    repoUrls=$repoUrls,http://download.sodius.com/
    features=$features,com.sodius.mdw.platform.doors.dxl.rcp.feature.group

    # cmake
    repoUrls=$repoUrls,"\"file:///$(cygpath -m "$download_path/eclipse_plugins/cmake4eclipse")\""
    features=$features,de.marw.cdt.cmake.feature.group
    repoUrls=$repoUrls,"\"file:///$(cygpath -m "$download_path/eclipse_plugins/cmakeed")\""
    features=$features,com.cthing.cmakeed.feature.feature.group

    # xml
    features=$features,org.eclipse.wst.xml_ui.feature.feature.group

    # git
    features=$features,org.eclipse.egit.feature.group

    # emf
    features=$features,org.eclipse.emf.sdk.feature.group
    features=$features,org.eclipse.emf.ecoretools.sdk.feature.group

    # ocl
    features=$features,org.eclipse.ocl.all.sdk.feature.group
    features=$features,org.eclipse.ocl.examples.feature.group

    # Visual C++ Support
    features=$features,org.eclipse.cdt.msw.feature.group


    echo "PATH=$(cygpath -w "$extract_path/jdk/bin");%PATH% & \""$(cygpath -w "$extract_path/eclipse/eclipsec.exe")"\" \
    -clean \
    -purgeHistory \
    -application org.eclipse.equinox.p2.director \
    -noSplash \
    -repository $repoUrls \
    -installIUs $features \
    -destination \""$(cygpath -w "$extract_path/eclipse")"\" \
    -profile SDKProfile \
    -vmargs -Declipse.p2.mirrors=false" | cmd
}

install_vscode_extensions() {

    mkdir -p "$extract_path/vscode/data/user-data/User"
    echo '{"http.proxyStrictSSL": false}' > "$extract_path/vscode/data/user-data/User/settings.json"
    "$extract_path/vscode/bin/code" --install-extension ms-vscode.cpptools \
            --install-extension ms-vscode.cmake-tools \
            --install-extension twxs.cmake \
            --install-extension ms-python.python \
            --install-extension yzhang.markdown-all-in-one \
            --install-extension vscodevim.vim \
            --install-extension EditorConfig.editorconfig \
            --install-extension johnstoncode.svn-scm \
            --install-extension donjayamanne.githistory \
            --install-extension eamodio.gitlens \
            --install-extension slevesque.shader \
            --install-extension shd101wyy.markdown-preview-enhanced \
            --install-extension torn4dom4n.latex-support
}

provision() {
    
    install_eclipse_plugins
    install_vscode_extensions

    # Cmder
    echo "Changing Cmder defaults"
    cp "$provision_path/ConEmu.xml" "$extract_path/cmder/vendor/ConEmu.xml.default"
    cp "$provision_path/ConEmu.xml" "$extract_path/cmder/vendor/conemu-maximus5/ConEmu.xml"
    cp "$provision_path/clink.lua" "$extract_path/cmder/vendor/clink.lua"
    cp "$wnix_path/provision/vimrc" "$extract_path/git/etc/vimrc"
    
    # Python 2, install and upgrade pip
    "$extract_path/python2/python" -m ensurepip
    "$extract_path/python2/python" -m pip install -U pip

    # Python 3, upgrade pip and install packages
    "$extract_path/python3/python" -m pip install -U pip
    "$extract_path/python3/python" -m pip install pyqt5 pyqtchart pyinstaller jinja2 numpy Pillow matplotlib aqtinstall xlrd pandas html-testRunner
    "$extract_path/python3/python" -m pip install "$download_path/$gdal_file" "$download_path/$rasterio_file"

    # download qt for msvc
    PATH="$extract_path/python3/Scripts":$PATH
    if [ ! -d "$extract_path/qt" ]; then
        aqt install -O "$extract_path/qt" -b http://ftp.fau.de/qtproject/ 5.15.2 windows desktop win32_msvc2019
        rm "$wnix_path/aqtinstall.log"
    fi

    # msys2
    pacman -S man vim rsync\
        mingw-w64-i686-toolchain mingw-w64-i686-cmake mingw-w64-i686-clang mingw-w64-i686-ninja\
        mingw-w64-i686-qt5 mingw-w64-i686-qt-creator\
        mingw-w64-i686-wget mingw-w64-i686-curl mingw-w64-i686-diffutils\
        mingw-w64-i686-glew mingw-w64-i686-freeglut mingw-w64-i686-doxygen mingw-w64-i686-imagemagick\
        --noconfirm --needed

    #miktex
    if [ ! -d "$extract_path/miktex" ]; then
        "$download_path/$miktex_file" --unattended --portable="$extract_path/miktex"
        cp -a "$download_path/miktex-repo" "$extract_path/miktex/miktex_repo"
    fi

    # # QtCreator fixes
    # #WNIX_DEV_DIR=`pwd -W`/msys2
    # #MSYS2_LINK=C:/msys64
    # #find /mingw64/share/qtcreator/QtProject/qtcreator/ -name *.xml -exec sed -i 's@'"$WNIX_DEV_DIR"'@'"$MSYS2_LINK"'@g' {} +
    # #sed -i 's@load(windows_vulkan_sdk)@load(win32/windows_vulkan_sdk)@g' /mingw64/share/qt5/mkspecs/common/windows-vulkan.conf
    
    touch "$provision_check"
}

compile() {
    mkdir -p "$extract_path/msvc"
    # Windows sets TMP/TEMP and MinGW sets tmp/temp and , if we don't unset TMP/TEMP cmake fails
    unset TMP
    unset TEMP

    # build freeglut msvc from source
    if [ ! -d "$extract_path/msvc/freeglut" ]; then
        cmake -G "$1" -A Win32 -B "$extract_path/src/freeglut/b1" -S "$extract_path/src/freeglut" -DCMAKE_INSTALL_PREFIX=install
        cmake --build "$extract_path/src/freeglut/b1"  --target install --config Release
        cp "$extract_path/src/freeglut/include/GL/glut.h" install/include/GL/glut.h
        mv install msvc/freeglut
        rm -r "$extract_path/src/freeglut/b1"
    fi

    # build glew msvc from source
    if [ ! -d "$extract_path/msvc/glew" ]; then
        if [ "$1" = "Visual Studio 12" ]; then
            cp "$extract_path/src/glew/build/cmake/CMakeLists.txt" "$extract_path/src/glew/build/cmake/CMakeLists.tmp"
            sed -i -e '/libvcruntime.lib/d' -e '/msvcrt.lib/d' "$extract_path/src/glew/build/cmake/CMakeLists.txt"
        fi
        cmake -G "$1" -A Win32 -B "$extract_path/src/glew/build/b1" -S "$extract_path/src/glew/build/cmake" -DCMAKE_INSTALL_PREFIX=install
        cmake --build "$extract_path/src/glew/build/b1"  --target install --config Release
        mv install msvc/glew
        rm -r "$extract_path/src/glew/build/b1"
        if [ "$1" = "Visual Studio 12" ]; then
            rm "$extract_path/src/glew/build/cmake/CMakeLists.txt"
            mv "$extract_path/src/glew/build/cmake/CMakeLists.tmp" "$extract_path/src/glew/build/cmake/CMakeLists.txt"
        fi
    fi
}

release() {

    # # Save pacman cache
    # cp -r /var/cache/pacman $download_path/cache
    
    # # Clean some things
    # pacman -Scc --noconfirm
    # rm -rf /home/*
    
    # # Kill weird tasks coming by first msys2 shell open
    # app1=dirmngr.exe
    # app2=gpg-agent.exe
    # taskkill //F //IM $app1 > /dev/null 2> /dev/null
    # taskkill //F //IM $app2 > /dev/null 2> /dev/null
    
    tar zcvf ../wnix-$1.tar.gz \
        --exclude="download"   \
        --exclude=".download"  \
        --exclude=".extract"   \
        --exclude=".provision" \
        --exclude=".git"       .
}

usage() {
    echo "Usage: `basename $0` [options]"
    echo
    echo "Options"
    echo "   -h, --help           Prints this help message"
    echo "   -d, --download       Downloads programs"
    echo "   -x, --extract        Extracts downloaded programs"
    echo "   -p, --provision      Provisions extracted programs"
    echo "   -c \"CMAKE_GENERATOR\", --compile \"CMAKE_GENERATOR\"        Compile FreeGLUT and GLEW from source"
    echo "   -r, --release        Releases a tarball with extracted programs"
}

main() {
    if [ $# -eq 0 ]; then
        echo "No option given"
        usage
        exit 1
    fi

    while [ $# -gt 0 ]; do
        key="$1"
        case "$key" in
            -d|--download)
                will_download=yes
                ;;
            -x|--extract)
                will_extract=yes
                ;;
            -p|--provision)
                will_provision=yes
                ;;
            -c|--compile)
                will_compile=yes
                shift
                generator="$1"
                case "$1" in
                    -d|--download|-x|--extract|-p|--provision|-c|--compile|-r|--release|-h|--help)
                        echo "You should provide a cmake generator for provision"
                        echo "Ex: \"Visual Studio 16\" for VS2019 or \"Visual Studio 12\" for VS2013"
                        exit 1
                        ;;
                esac
                ;;
            -r|--release)
                will_release=yes
                ;;
            -h|--help)
                usage
                exit 0
                ;;
            *)
                echo "Unknown option: $key"
                usage
                exit 1
                ;;
        esac
        shift
    done

    if [ ! -z $will_download ]; then
        download
        echo "Download done!"
    fi

    if [ ! -z $will_extract ]; then
        if [ -f "$download_check" ]; then
            extract
            echo "Extraction done!"
        else
            echo "Nothing to extract. You should first download!"
            usage
            exit 1
        fi
    fi
    
    if [ ! -z $will_provision ]; then
        if [ -f "$extract_check" ]; then
            provision
            echo "Provision done!"
        else
            echo "Nothing to provision. You should first extract the programs!"
            usage
            exit 1
        fi
    fi

    if [ ! -z $will_compile ]; then
        if [ -f "$provision_check" ]; then
            compile "$generator"
            echo "Compile done!"
        else
            echo "Cannot compile. You should first provision the programs!"
            usage
            exit 1
        fi
    fi

    if [ ! -z $will_release ]; then
        if [ -f "$provision_check" ]; then
            release $wnix_version
            echo "Release done!"
        else
            echo "Cannot release. You should first provision the programs!"
            usage
            exit 1
        fi
    fi
}

main "$@"
