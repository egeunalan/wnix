@echo off

:: Setup environment variables
call "%~dp0setenv.bat"

:: Get argument
set app_name=%1

:: Start Cmder
if %app_name% == cmder start "" "%CMDER_HOME%\Cmder.exe"

:: Start Eclipse
if %app_name% == eclipse start "" "%ECLIPSE_HOME%\eclipse.exe"

:: Start VSCode
if %app_name% == code start "" "%VSCODE_HOME%\Code.exe"

:: Start Qt Creator
if %app_name% == qtcreator start "" "%MINGW32_HOME%\bin\qtcreator.exe"

:: Start Qt Designer
if %app_name% == qtdesigner start "" "%MINGW32_HOME%\bin\designer.exe"
